#pragma once

#include "minilibx-linux/mlx.h"
#include "utils.hpp"
#include "color.hpp"

#include <exception>
#include <utility>


class MlxWindow
{
public:
    /* virtial methods */
    virtual void on_expose() {};
    virtual void on_key(int) {};
    virtual void on_mouse(int, int, int) {};
    virtual void on_idle() {};

    MlxWindow(void* mlx_ptr, int size_x, int size_y, const char* title) :
        mlx_ptr(mlx_ptr),
        size_x(size_x),
        size_y(size_y)
    {
        /* Why not const char*? */
        mlx_win = mlx_new_window(mlx_ptr, size_x, size_y, const_cast<char*>(title));
        if (!mlx_win) {
            throw std::runtime_error("mlx_new_window returned nullptr");
        }
        make_hooks();
    }

    MlxWindow() : mlx_ptr(nullptr), mlx_win(nullptr), size_x(0), size_y(0) {}
    /* Forbid copying of window */
    MlxWindow(const MlxWindow&) = delete;
    MlxWindow& operator=(const MlxWindow&) = delete;

    /* Add move semantic */
    MlxWindow(MlxWindow&& window) : mlx_ptr(nullptr), mlx_win(nullptr) {
        *this = std::move(window);
    }

    MlxWindow& operator=(MlxWindow&& window) {
        if (this != &window) {
            destroy();
            mlx_ptr = window.mlx_ptr;
            mlx_win = window.mlx_win;
            size_x = window.size_x;
            size_y = window.size_y;
            window.mlx_win = nullptr;
            make_hooks();
        }
        return *this;
    }

    ~MlxWindow() {
        destroy();
    }

    inline void make_hooks();

    void clear() const {
        if (!mlx_win)
            throw std::runtime_error("trying to clean not initizalized window"); 
        mlx_clear_window(mlx_ptr, mlx_win);
    }

    explicit operator bool() const { 
        return mlx_win; 
    }

    void loop() const { 
        mlx_loop(mlx_ptr);
    }

    void destroy() {
        if (mlx_win) {
            mlx_destroy_window(mlx_ptr, mlx_win);
            mlx_win = nullptr;
        }
    }

    void put_pixel(int x, int y, const Color& color) const {
        Color saturated = color.saturate();
        mlx_pixel_put(mlx_ptr, mlx_win, x, y, (saturated.r_8bit() << 16) | (saturated.g_8bit() << 8) | (saturated.b_8bit()));
    }

    int get_size_x() const {
        return size_x;
    }

    int get_size_y() const {
        return size_y;
    }

protected:
    void* mlx_ptr;
    void* mlx_win;

    int size_x, size_y;
};

inline int expose_hook(MlxWindow *w) {
    w->on_expose();
    return 0;
}

inline int key_hook(int keycode, MlxWindow *w) {
    w->on_key(keycode);
    return 0;
}

inline int mouse_hook(int button, int x, int y, MlxWindow *w) {
    w->on_mouse(button, x, y);
    return 0;
}

inline int loop_hook(MlxWindow *w) {
    w->on_idle();
    return 0;
}

// int close_hook(void*) {
//     std::cerr << "sosi huy" << std::endl;
//     return 0;
// }

void MlxWindow::make_hooks() {
    /* Why not correct function pointers?? */
    using FuncType = int (*)();
    mlx_expose_hook(mlx_win, reinterpret_cast<FuncType>(expose_hook), this);
    mlx_key_hook(mlx_win, reinterpret_cast<FuncType>(key_hook), this);
    mlx_mouse_hook(mlx_win, reinterpret_cast<FuncType>(mouse_hook), this);
    mlx_loop_hook(mlx_win, reinterpret_cast<FuncType>(loop_hook), this);
    // mlx_hook(mlx_win, 2, 1, reinterpret_cast<FuncType>(close_hook), nullptr);
}
