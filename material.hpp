#pragma once

#include "vector.hpp"

#include <vector>
#include <cmath>

struct RoughMaterial {
    double ambient_refl = 0.0;
    double diffuse_refl = 0.0;
    double specular_refl = 0.0;
    double shininess = 1.0;

    Vector3d color;

    struct LightInfo {
        Vector3d direction;
        Vector3d color;
    };

    Vector3d get_ray_light(
        const std::vector<LightInfo>& lights,
        const Vector3d& normal,
        const Vector3d& viewer,
        const Vector3d& ambient_color
    ) const {
        Vector3d result = color * ambient_color * ambient_refl;

        for (const LightInfo& light : lights) {
            /* Diffusive part */
            result += light.color * color * (diffuse_refl * std::max(dot_product(light.direction.normalize(), normal.normalize()), 0.0));

            /* Specular part */
            Vector3d light_reflection = -light.direction + 2 * light.direction.project_on(normal);
            double spec = std::max(dot_product(light_reflection.normalize(), viewer.normalize()), 0.0);
            result += light.color * (specular_refl * std::pow(spec, shininess));
        }
        return result;
    }
};
