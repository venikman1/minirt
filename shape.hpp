#pragma once

#include "vector.hpp"

#include <cmath>
#include <optional>

class Shape {
public:
    struct RayCollision {
        Vector3d point;
        Vector3d normal;
    };

    virtual ~Shape() = default;
    virtual std::optional<RayCollision> cast_ray(const Vector3d& start_point, const Vector3d& direction) const = 0;
};

struct Sphere : virtual public Shape {
    std::optional<RayCollision> cast_ray(const Vector3d& start_point, const Vector3d& direction) const override {
        Vector3d to_center = center - start_point;
        Vector3d proj = to_center.project_on(direction);
        Vector3d closest_point = start_point + proj;

        Vector3d h = center - closest_point;
        double dd = radius * radius - dot_product(h, h);
        if (dot_product(proj, direction) < 0 || dd < 0)
            return std::nullopt;
        
        double f = std::sqrt(dd);
        RayCollision result;
        result.point = closest_point - direction.normalize() * f;
        result.normal = (result.point - center).normalize();
        return result;
    }

    Vector3d center;
    Vector3d::value_type radius;
};

struct Plane : virtual public Shape {
    Plane(const Vector3d& normal, const Vector3d& pos) : normal(normal) {
        D = -(dot_product(normal, pos));
    }

    Plane() = default;

    std::optional<RayCollision> cast_ray(const Vector3d& start_point, const Vector3d& direction) const override {
        RayCollision result;

        double d = dot_product(normal, direction);
        if (std::abs(d) < 1e-5) {
            return std::nullopt;
        }
        double t = -(D + dot_product(start_point, normal)) / d;
        result.point = start_point + t * direction;
        if (dot_product(result.point - start_point, direction) < 0)
            return std::nullopt;
        result.normal = (dot_product(direction, normal) > 0) ? -normal.normalize() : normal.normalize();
        return result;
    }

    Vector3d normal;
    Vector3d::value_type D;
};
