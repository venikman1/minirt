#pragma once

#include "vector.hpp"

#include <utility>
#include <cmath>

class Camera {
public:
    struct Ray {
        Vector3d start_point;
        Vector3d direction;
    };

    virtual Ray get_ray(int x, int y, int resolution_x, int resolution_y) = 0;

    virtual ~Camera() = default;
};

class RectangleCamera : virtual public Camera {
public:
    RectangleCamera(Vector3d pos, const Vector3d& dir, double size_x, double size_y, double hor_angle,
                    const Vector3d& vertical_vector = {0.0, 0.0, 1.0}) :
        position(std::move(pos)),
        size_x(size_x),
        size_y(size_y)
    {
        central_ray = dir.normalize() * (size_x / (2 * std::tan(hor_angle / 2.0)));
        x_norm = cross_product(dir, vertical_vector).normalize();
        y_norm = cross_product(dir, x_norm).normalize();
    }

    Ray get_ray(int x, int y, int resolution_x, int resolution_y) override {
        double x_part = static_cast<double>(x) / resolution_x * size_x - size_x / 2.0;
        double y_part = static_cast<double>(y) / resolution_y * size_y - size_y / 2.0;

        Ray ray;
        ray.direction = central_ray + x_norm * x_part + y_norm * y_part;
        ray.start_point = position;
        return ray;
    }

private:
    Vector3d position;
    Vector3d central_ray;
    double size_x, size_y;

    Vector3d x_norm, y_norm;
};
