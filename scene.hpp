#pragma once

#include "shape.hpp"
#include "material.hpp"
#include "camera.hpp"
#include "window.hpp"
#include "color.hpp"
#include "light.hpp"

#include <vector>
#include <memory>
#include <tuple>
#include <optional>

class Scene {
public:
    struct SceneObject {
        std::unique_ptr<RoughMaterial> material;
        std::unique_ptr<Shape> shape;
        double rough = 1.0;
        double reflective = 0.0;
        double transparent = 0.0;
        Vector3d reflective_color = WHITE;
        Vector3d tranparent_color = WHITE;

        SceneObject(RoughMaterial* material, Shape* shape) : material(material), shape(shape) {}
        SceneObject() = default;
    };

    void render(const MlxWindow& window) {
        for (int x = 0; x < window.get_size_x(); ++x) {
            for (int y = 0; y < window.get_size_y(); ++y) {
                window.put_pixel(x, y, antialise(x, y, window.get_size_x(), window.get_size_y()));
            }
        }
    }

    Vector3d antialise(int x, int y, int size_x, int size_y) {
        Vector3d result = {0.0, 0.0, 0.0};
        for (int xa = 0; xa < antialiasing; ++xa) {
            for (int ya = 0; ya < antialiasing; ++ya) {
                Camera::Ray ray = camera->get_ray(x * antialiasing + xa, y * antialiasing + ya,
                                                  size_x * antialiasing, size_y * antialiasing);
                result += trace_ray(ray.start_point, ray.direction, 7);
            }
        }
        return result / (antialiasing * antialiasing);
    }

    std::optional<std::tuple<SceneObject&, Shape::RayCollision>> trace_first_collision(
        const Vector3d& start_point,
        const Vector3d& ray_direction
    ) {
        int closest_object = -1;
        double closest_distance = 0.0;
        Shape::RayCollision closest_vertex = {Vector3d(), Vector3d()};

        for (size_t i = 0; i < objects.size(); ++i) {
            auto vertex = objects[i].shape->cast_ray(start_point, ray_direction);
            if (vertex) {
                double distance = (vertex->point - start_point).length();
                if (closest_object == -1 || distance < closest_distance) {
                    closest_distance = distance;
                    closest_object = i;
                    closest_vertex = *vertex;
                }
            }
        }

        if (closest_object == -1) {
            return std::nullopt;
        }

        return std::tuple<SceneObject&, Shape::RayCollision>(objects[closest_object], closest_vertex);
    }

    Vector3d trace_ray(const Vector3d& start_point, const Vector3d& ray_direction, int recursion_limit = 0) {
        auto coll = trace_first_collision(start_point, ray_direction);

        if (!coll)
            return BLACK;

        SceneObject& object = std::get<0>(*coll);
        Shape::RayCollision vertex = std::get<1>(*coll);

        Vector3d rough_color = get_rough_color(start_point, object, vertex);
        Vector3d reflection_color = BLACK;
        if (recursion_limit > 0 && object.reflective > 0.0) {
            Vector3d reflection_dir = (vertex.point - start_point) - 2 * (vertex.point - start_point).project_on(vertex.normal);
            reflection_color = trace_ray(vertex.point + reflection_dir * 0.00001, reflection_dir, recursion_limit - 1);
        }
        return rough_color * object.rough + reflection_color * object.reflective_color * object.reflective;
    }

    Vector3d get_rough_color(const Vector3d& start_point, SceneObject& object, Shape::RayCollision& vertex) {
        RoughMaterial& material = *(object.material);
        std::vector<RoughMaterial::LightInfo> light_infos;
        for (auto& light : lights) {
            auto light_coll = trace_first_collision(light.position, vertex.point - light.position);
            if (!light_coll || (std::get<1>(*light_coll).point - vertex.point).length() > 1e-6) {
                continue;
            }
            RoughMaterial::LightInfo info;
            info.direction = light.position - vertex.point;
            double dist = info.direction.length();
            info.color = light.color / (dist * dist);
            light_infos.push_back(info);
        }
        return material.get_ray_light(light_infos, vertex.normal, start_point - vertex.point, WHITE);
    }

    
    std::vector<Light> lights;
    std::vector<SceneObject> objects;
    std::unique_ptr<Camera> camera;
    int antialiasing = 1;
};