#pragma once

#include <array>
#include <utility>
#include <cmath>
#include <type_traits>

template <class T, int LENGTH>
class Vector : public std::array<T, LENGTH> {
public:
    Vector operator+() const {
        return *this;
    }

    Vector operator-() const {
        Vector result;
        for (size_t i = 0; i < LENGTH; ++i) {
            result[i] = -(*this)[i];
        }
        return result;
    }

    Vector& operator+=(const Vector& other) {
        for (size_t i = 0; i < LENGTH; ++i) {
            (*this)[i] += other[i];
        }
        return *this;
    }

    Vector& operator-=(const Vector& other) {
        for (size_t i = 0; i < LENGTH; ++i) {
            (*this)[i] -= other[i];
        }
        return *this;
    }

    Vector& operator*=(const T& other) {
        for (size_t i = 0; i < LENGTH; ++i) {
            (*this)[i] *= other;
        }
        return *this;
    }

    Vector& operator*=(const Vector& v) {
        for (size_t i = 0; i < LENGTH; ++i) {
            (*this)[i] *= v[i];
        }
        return *this;
    }

    Vector& operator/=(const T& other) {
        for (size_t i = 0; i < this->size(); ++i) {
            (*this)[i] /= other;
        }
        return *this;
    }

    Vector& operator/=(const Vector& v) {
        for (size_t i = 0; i < LENGTH; ++i) {
            (*this)[i] /= v[i];
        }
        return *this;
    }

    friend Vector operator+(const Vector& v1, const Vector& v2) {
        Vector result = v1;
        result += v2;
        return result;
    }

    friend Vector operator-(const Vector& v1, const Vector& v2) {
        Vector result = v1;
        result -= v2;
        return result;
    }

    friend Vector operator*(const Vector& v1, const T& t) {
        Vector result = v1;
        result *= t;
        return result;
    }

    friend Vector operator*(const Vector& v1, const Vector& v2) {
        Vector result = v1;
        result *= v2;
        return result;
    }

    friend Vector operator/(const Vector& v1, const Vector& v2) {
        Vector result = v1;
        result /= v2;
        return result;
    }

    friend Vector operator*(const T& t, const Vector& v1) {
        return v1 * t;
    }

    friend Vector operator/(const Vector& v1, const T& t) {
        Vector result = v1;
        result /= t;
        return result;
    }

    template <std::enable_if_t<(LENGTH >= 1), int> = 0>
    T& x() {
        return (*this)[0];
    }

    template <std::enable_if_t<(LENGTH >= 2), int> = 0>
    T& y() {
        return (*this)[1];
    }

    template <std::enable_if_t<(LENGTH >= 3), int> = 0>
    T& z() {
        return (*this)[2];
    }

    template <std::enable_if_t<(LENGTH >= 1), int> = 0>
    const T& x() const {
        return (*this)[0];
    }

    template <std::enable_if_t<(LENGTH >= 2), int> = 0>
    const T& y() const {
        return (*this)[1];
    }

    template <std::enable_if_t<(LENGTH >= 3), int> = 0>
    const T& z() const {
        return (*this)[2];
    }

    friend T dot_product(const Vector& a, const Vector& b) {
        T result = 0;
        for (size_t i = 0; i < LENGTH; ++i) {
            result += a[i] * b[i];
        }
        return result;
    }

    template <std::enable_if_t<(LENGTH == 3), int> = 0>
    friend Vector cross_product(const Vector& a, const Vector& b) {
        return Vector({a[1] * b[2] - a[2] * b[1], -(a[0] * b[2] - a[2] * b[0]), a[0] * b[1] - a[1] * b[0]});
    }

    double length() const {
        return std::sqrt(dot_product(*this, *this));
    }

    Vector normalize() const {
        return (*this) / length();
    }

    Vector project_on(const Vector& v) const {
        return v * (dot_product(*this, v) / dot_product(v, v));
    }
};

using Vector3d = Vector<double, 3>;

