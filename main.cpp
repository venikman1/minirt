#include "scene.hpp"


#include <chrono>
#include <thread>
#include <vector>

class MyWindow : public MlxWindow {
    using MlxWindow::MlxWindow;
    
    void on_idle() override {
        std::cerr << "rofl" << std::endl;
    }

    void on_expose() override {
        std::cerr << "rofl expose" << std::endl;
        Scene scene;
        scene.camera.reset(new RectangleCamera({0.0, -2.0, 1.0}, {0.0, 1.0, 0.0}, size_x, size_y,
                                                60.0 / 180.0 * 3.14));
        RoughMaterial* mt = new RoughMaterial;
        mt->ambient_refl = 0.2;
        mt->color = GREEN + BLUE;
        mt->diffuse_refl = 1.0;
        mt->specular_refl = 0.3;
        mt->shininess = 8.0;

        Sphere* sph = new Sphere;
        sph->center = {1.0, 5.0, 1.5};
        sph->radius = 1.0;
        scene.objects.emplace_back(mt, sph);
        scene.objects.back().reflective = 0.8;
        scene.objects.back().rough = 0.0;
        scene.objects.back().reflective_color = RED;// GREEN + BLUE;

        mt = new RoughMaterial;
        mt->ambient_refl = 0.2;
        mt->color = RED + BLUE;
        mt->diffuse_refl = 1.0;
        mt->specular_refl = 0.6;
        mt->shininess = 20.0;

        sph = new Sphere();
        sph->center = {-1.5, 5.0, 1.5};
        sph->radius = 1.0;
        scene.objects.emplace_back(mt, sph);
        scene.objects.back().reflective = 0.8;
        scene.objects.back().rough = 0.0;
        scene.objects.back().reflective_color = WHITE;

        mt = new RoughMaterial;
        mt->ambient_refl = 0.2;
        mt->color = RED + GREEN;
        mt->diffuse_refl = 1.0;
        mt->specular_refl = 0.6;
        mt->shininess = 8.0;
        scene.objects.emplace_back(mt, new Plane({0.0, -1.0, 0.0}, {0.0, 7.0, 0.0}));

        mt = new RoughMaterial;
        mt->ambient_refl = 0.2;
        mt->color = WHITE * 0.5;
        mt->diffuse_refl = 1.0;
        mt->specular_refl = 0.1;
        mt->shininess = 8.0;
        scene.objects.emplace_back(mt, new Plane({1.0, 0.0, 0.0}, {-4.0, 0.0, 0.0}));

        mt = new RoughMaterial;
        mt->ambient_refl = 0.2;
        mt->color = WHITE * 0.5;
        mt->diffuse_refl = 1.0;
        mt->specular_refl = 0.1;
        mt->shininess = 8.0;
        scene.objects.emplace_back(mt, new Plane({1.0, 0.0, 0.0}, {4.0, 0.0, 0.0}));

        mt = new RoughMaterial;
        mt->ambient_refl = 0.2;
        mt->color = RED;
        mt->diffuse_refl = 1.0;
        mt->specular_refl = 0.1;
        mt->shininess = 8.0;
        scene.objects.emplace_back(mt, new Plane({0.0, 1.0, 0.0}, {0.0, -3.0, 0.0}));

        mt = new RoughMaterial;
        mt->ambient_refl = 0.2;
        mt->color = BLUE;
        mt->diffuse_refl = 1.0;
        mt->specular_refl = 0.1;
        mt->shininess = 8.0;
        scene.objects.emplace_back(mt, new Plane({0.0, 0.0, 1.0}, {0.0, 0.0, 10.0}));
        
        mt = new RoughMaterial;
        mt->ambient_refl = 0.6;
        mt->color = GREEN;
        mt->diffuse_refl = 1.0;
        mt->specular_refl = 0.6;
        mt->shininess = 10.0;

        scene.objects.emplace_back(mt, new Plane({0.0, 0.0, 1.0}, {0.0, 0.0, 0.0}));

        

        Light light;
        light.position = {-3.0, 3.0, 3.0};
        light.color = WHITE * 5.0;
        scene.lights.push_back(light);

        light.position = {3.0, 3.0, 3.0};
        light.color = WHITE * 5.0;
        scene.lights.push_back(light);

        scene.antialiasing = 8;
        scene.render(*this);
    }

    void on_mouse(int button,int x,int y) override {
        std::cerr << "rofl pressed " << button << " " << x << " " << y << std::endl;
    }

    void on_key(int keycode) override {
        std::cerr << "rofl key " << keycode << std::endl;
        // if (keycode == 65362) {
        //     intensivity += 2.0;
        // }
        // if (keycode == 65364) {
        //     intensivity -= 2.0;
        //     intensivity = std::max(intensivity, 0.0);
        // }

        // if (keycode == 65361) {
        //     spectacular /= 2;
        // }
        // if (keycode == 65363) {
        //     spectacular *= 2;
        // }
        on_expose();
    }

    double intensivity = 10.0;
    double spectacular = 1.0;

    
};

int main() {
    void* mlx = mlx_init();
    if (!mlx) {
        exit_with_error("Error with mlx_init");
    }
    std::vector<MlxWindow> windows;
    MyWindow w(mlx, 900, 900, "Holo4ka");

    MyWindow woof = std::move(w);
    woof.loop();
}