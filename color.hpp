#pragma once

#include "vector.hpp"

#include <exception>
#include <algorithm>
#include <cmath>
#include <utility>

class Color {
public:
    Color(Vector3d vec) : vec(std::move(vec)) {}

    operator Vector3d() const {
        return vec;
    }

    Vector3d::value_type& r() {
        return vec.x();
    }

    Vector3d::value_type& g() {
        return vec.y();
    }

    Vector3d::value_type& b() {
        return vec.z();
    }

    Vector3d::value_type r() const {
        return vec.x();
    }

    Vector3d::value_type g() const {
        return vec.y();
    }

    Vector3d::value_type b() const {
        return vec.z();
    }

    unsigned int r_8bit() const {
        return to_8_bit(r());
    }

    unsigned int g_8bit() const {
        return to_8_bit(g());
    }

    unsigned int b_8bit() const {
        return to_8_bit(b());
    }

    static unsigned int to_8_bit(Vector3d::value_type num) {
        if (num < 0 || num > 1.0) {
            throw std::logic_error("Color value is not in interval [0.0; 1.0]");
        }
        return static_cast<unsigned int>(std::round(255u * num));
    }

    static Vector3d::value_type saturate_channel(Vector3d::value_type num) {
        return std::max(std::min(num, 1.0), 0.0);
    }

    Color saturate() const {
        return Color({saturate_channel(r()), saturate_channel(g()), saturate_channel(b())});
    }
    // static Color make_color()


private:
    Vector3d vec;
};

inline const Vector3d RED{1.0, 0.0, 0.0};
inline const Vector3d GREEN{0.0, 1.0, 0.0};
inline const Vector3d BLUE{0.0, 0.0, 1.0};

inline const Vector3d BLACK{0.0, 0.0, 0.0};
inline const Vector3d WHITE{1.0, 1.0, 1.0};
