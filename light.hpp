#pragma once

#include "vector.hpp"

struct Light {
    Vector3d position;
    Vector3d color;
};