#pragma once

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

void exit_with_error(const char* error) {
    std::cerr << error << std::endl;
    exit(EXIT_FAILURE);
}

void exit_with_perror(const char* error) {
    perror(error);
    exit(EXIT_FAILURE);
}
